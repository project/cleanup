<?php

namespace Drupal\Tests\cleanup\Unit;

use Drupal\cleanup\CleanupListBuilder;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\cleanup\CleanupListBuilder
 *
 * @group Cleanup
 */
class CleanupListBuilderTest extends UnitTestCase {

  /**
   * Instance of CleanupListBuilder.
   *
   * @var \Drupal\cleanup\CleanupListBuilder
   */
  protected $listBuilder;

  /**
   * Instance of Cleanup.
   *
   * @var \Drupal\cleanup\CleanupInterface
   */
  protected $entity;

  /**
   * Setup.
   */
  public function setup() {

    // String Translation Service.
    $mockStringTranslationService = $this->getMockBuilder('\Drupal\Core\StringTranslation\TranslationInterface')
      ->getMock();
    $mockStringTranslationService->expects($this->any())
      ->method('translate')
      ->willReturn('Translated');

    // Mock database query for EntityListBuilder::getEntityIds()
    $mockQuery = $this->getMockBuilder('\Drupal\Core\Entity\Query\QueryInterface')
      ->getMock();
    $mockQuery->expects($this->any())
      ->method('sort')
      ->withAnyParameters()
      ->willReturn($mockQuery);
    $mockQuery->expects($this->any())
      ->method('pager')
      ->withAnyParameters()
      ->willReturn($mockQuery);
    $mockQuery->expects($this->any())
      ->method('execute')
      ->willReturn([]);

    // Storage.
    $mockStorage = $this->getMockBuilder('\Drupal\Core\Config\Entity\ConfigEntityStorage')
      ->disableOriginalConstructor()
      ->getMock();
    $mockStorage->expects($this->any())
      ->method('getQuery')
      ->willReturn($mockQuery);
    $mockStorage->expects($this->any())
      ->method('loadMultipleOverrideFree')
      ->withAnyParameters()
      ->willReturn([]);

    $mockEntityTypeManager = $this->getMockBuilder('\Drupal\Core\Entity\EntityTypeManager')
      ->disableOriginalConstructor()
      ->getMock();
    $mockEntityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('cleanup')
      ->willReturn($mockStorage);

    $mockModuleHandler = $this->getMockBuilder('\Drupal\core\extension\ModuleHandler')
      ->disableOriginalConstructor()
      ->setMethods(['invokeAll', 'alter'])
      ->getMock();

    $mockModuleHandler->expects($this->any())
      ->method('invokeAll')
      ->withAnyParameters()
      ->willReturn([]);

    $mockModuleHandler->expects($this->any())
      ->method('alter');

    $mockUrlGenerator = $this->getMockBuilder('\Drupal\Core\Routing\UrlGeneratorInterface')
      ->getMock();
    $mockUrlGenerator->expects($this->any())
      ->method('generateFromRoute')
      ->withAnyParameters()
      ->willReturn('/mock/url/generated/route');

    $container = $this->getMockBuilder('\Symfony\Component\DependencyInjection\ContainerInterface')
      ->getMock();

    // returnValueMap didn't work for me, perhaps because it doesn't expect
    // variables?
    $callback = function ($key) use ($mockEntityTypeManager, $mockStringTranslationService, $mockModuleHandler, $mockUrlGenerator) {
      $map = [
        'entity_type.manager' => $mockEntityTypeManager,
        'string_translation' => $mockStringTranslationService,
        'module_handler' => $mockModuleHandler,
        'url_generator' => $mockUrlGenerator,
      ];
      return $map[$key];
    };

    $container->expects($this->any())
      ->method('get')
      ->willReturnCallback($callback);

    $entityType = $this->getMockBuilder('\Drupal\Core\Entity\EntityTypeInterface')
      ->getMock();
    $entityType->expects($this->any())
      ->method('id')
      ->willReturn('cleanup');
    $entityType->expects($this->any())
      ->method('getListCacheContexts')
      ->willReturn([]);
    $entityType->expects($this->any())
      ->method('getListCacheTags')
      ->willReturn([]);
    $entityType->expects($this->any())
      ->method('getClass')
      ->willReturn('\Drupal\Tests\cleanup\Unit\MockEntitySorter');

    $this->listBuilder = CleanupListBuilder::createInstance($container, $entityType);
    $this->listBuilder->setStringTranslation($mockStringTranslationService);

    // Mock entity creation.
    $this->entity = $this->getMockBuilder('\Drupal\cleanup\Entity\Cleanup')
      ->disableOriginalConstructor()
      ->getMock();

    $this->entity->expects($this->any())
      ->method('label')
      ->willReturn('Test Entity Label');

    $this->entity->expects($this->any())
      ->method('toUrl')
      ->with('run')
      ->willReturn('/link/to/run/cleanup');

    $tasks = $this->getMockBuilder('\Drupal\cleanup\CleanupTaskPluginCollection')
      ->disableOriginalConstructor()
      ->getMock();

    $this->entity->expects($this->any())
      ->method('getTasks')
      ->willReturn($tasks);

    \Drupal::setContainer($container);
  }

  /**
   * Build header produces expected render array.
   *
   * @test
   */
  public function buildHeaderProducesExpectedRenderArray() {

    $result = $this->listBuilder->buildHeader();

    $this->assertArrayHasKey('label', $result);
    $this->assertInstanceOf('\Drupal\Core\StringTranslation\TranslatableMarkup', $result['label']);
    $this->assertEquals('Cleanup name', $result['label']->getUntranslatedString());
    $this->assertArrayHasKey('operations', $result);
    $this->assertInstanceOf('\Drupal\Core\StringTranslation\TranslatableMarkup', $result['operations']);
    $this->assertEquals('Operations', $result['operations']->getUntranslatedString());
  }

  /**
   * Build row produces expected render array.
   *
   * @test
   */
  public function buildRowProducesExpectedRenderArrayWithOperations() {

    $this->entity->getTasks()->expects($this->any())
      ->method('count')
      ->willReturn(1);

    $result = $this->listBuilder->buildRow($this->entity);

    $runLinkTitle = $result['operations']['data']['#links']['run']['title'] ?: NULL;
    $this->assertInstanceOf('\Drupal\Core\StringTranslation\TranslatableMarkup', $runLinkTitle);

    if (!empty($runLinkTitle)) {
      $this->assertEquals('Run', $runLinkTitle->getUntranslatedString());
    }

    unset($result['operations']['data']['#links']['run']['title']);
    $this->assertArrayEquals([
      'label' => 'Test Entity Label',
      'operations' => [
        'data' => [
          '#type' => 'operations',
          '#links' => [
            'run' => [
              'weight' => 200,
              'url' => '/link/to/run/cleanup',
            ],
          ],
        ],
      ],
    ], $result);
  }

  /**
   * Build row produces expected render array when no operations.
   *
   * @test
   */
  public function buildRowProducesExpectedRenderArrayWithNoOperations() {

    $this->entity->getTasks()->expects($this->any())
      ->method('count')
      ->willReturn(0);

    $result = $this->listBuilder->buildRow($this->entity);

    $this->assertArrayEquals([
      'label' => 'Test Entity Label',
      'operations' => [
        'data' => [
          '#type' => 'operations',
          '#links' => [],
        ],
      ],
    ], $result);
  }

  /**
   * Render invokes parent and adds expected empty text.
   *
   * @test
   */
  public function renderInvokesParentAndAddsExpectedEmptyText() {

    $this->entity->getTasks()->expects($this->any())
      ->method('count')
      ->willReturn(0);

    $result = $this->listBuilder->render();

    $this->assertInstanceOf('\Drupal\Core\StringTranslation\TranslatableMarkup',
      $result['table']['#empty']);
    $this->assertEquals('There are currently no task lists. <a href=":url">Add a new one</a>.',
      $result['table']['#empty']->getUntranslatedString());

    // Remove the translatable markup that has already been checked above
    // or in previous tests.
    unset($result['table']['#header']['label']);
    unset($result['table']['#header']['operations']);
    unset($result['table']['#empty']);

    $this->assertArrayEquals([
      'table' => [
        '#type' => 'table',
        '#header' => [],
        '#title' => NULL,
        '#rows' => [],
        '#cache' => [
          'contexts' => [],
          'tags' => [],
        ],
      ],
      'pager' => [
        '#type' => 'pager',
      ],
    ], $result);
  }

}
