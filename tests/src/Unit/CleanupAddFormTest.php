<?php

namespace Drupal\Tests\cleanup\Unit;

use Drupal\Core\Form\FormState;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for the form for adding a new cleanup.
 */
class CleanupAddFormTest extends UnitTestCase {

  /**
   * The submit form calls parent handler and adds a translatable message.
   *
   * Parent calls buildEntity so we need to mock that. We can therefore check
   * that the class does indeed invoke the parent submit handler but expecting
   * buildEntity to be called once.
   *
   * @test
   */
  public function cleanupAddFormSubmitHandlerAddsTranslatableStatusMessage() {

    $mockStringTranslationService = $this->getMockBuilder('Drupal\Core\StringTranslation\TranslationInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $container = $this->getMockBuilder('\Symfony\Component\DependencyInjection\ContainerInterface')
      ->getMock();
    $container->expects($this->once())
      ->method('get')
      ->with('string_translation')
      ->willReturn($mockStringTranslationService);
    \Drupal::setContainer($container);

    $mockMessenger = $this->getMockBuilder('Drupal\Core\Messenger\MessengerInterface')
      ->getMock();
    $mockMessenger->expects($this->once())
      ->method('addStatus');

    $mockEntity = $this->getMockBuilder('Drupal\Core\Entity\ContentEntityInterface')
      ->getMock();
    $mockEntity->expects($this->any())
      ->method('label')
      ->willReturn('My entity');

    $mock = $this->getMockBuilder('Drupal\cleanup\Form\CleanupAddForm')
      ->disableOriginalConstructor()
      ->setMethodsExcept(['submitForm'])
      ->getMock();
    $mock->expects($this->once())
      ->method('messenger')
      ->willReturn($mockMessenger);
    $mock->expects($this->once())
      ->method('buildEntity')
      ->willReturn($mockEntity);

    $mockFormState = new FormState();
    $form = [];
    $mock->submitForm($form, $mockFormState);
  }

  /**
   * The actions helper invokes its parent and sets the text.
   *
   * @test
   */
  public function actionsHelperInvokesParentAndSetsText() {

    $mockStringTranslationService = $this->getMockBuilder('Drupal\Core\StringTranslation\TranslationInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $container = $this->getMockBuilder('\Symfony\Component\DependencyInjection\ContainerInterface')
      ->getMock();
    $container->expects($this->once())
      ->method('get')
      ->with('string_translation')
      ->willReturn($mockStringTranslationService);
    \Drupal::setContainer($container);

    $mockEntity = $this->getMockBuilder('Drupal\Core\Entity\ContentEntityInterface')
      ->getMock();
    $mockEntity->expects($this->any())
      ->method('label')
      ->willReturn('My entity');
    $mockEntity->expects($this->once())
      ->method('isNew')
      ->willReturn(TRUE);

    $mock = $this->getMockBuilder('Drupal\cleanup\Form\CleanupAddForm')
      ->disableOriginalConstructor()
      ->setMethodsExcept(['actions', 'setEntity'])
      ->getMock();

    $mock->setEntity($mockEntity);

    $mockFormState = new FormState();
    $form = [];
    $result = $mock->actions($form, $mockFormState);

    $this->assertInstanceOf('\Drupal\Core\StringTranslation\TranslatableMarkup', $result['submit']['#value']);
    $this->assertEquals('Create new cleanup task list', $result['submit']['#value']->getUntranslatedString());
  }

}
