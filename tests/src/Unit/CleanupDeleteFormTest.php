<?php

namespace Drupal\Tests\cleanup\Unit;

use Drupal\cleanup\Form\CleanupDeleteForm;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for the form for adding a new cleanup.
 */
class CleanupDeleteFormTest extends UnitTestCase {

  /**
   * The delete form getQuestion hook returns a translatable message.
   *
   * @test
   */
  public function cleanupDeleteFormQuestionIsTranslatable() {

    $mockStringTranslationService = $this->getMockBuilder('Drupal\Core\StringTranslation\TranslationInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $container = $this->getMockBuilder('\Symfony\Component\DependencyInjection\ContainerInterface')
      ->getMock();
    $container->expects($this->any())
      ->method('get')
      ->with('string_translation')
      ->willReturn($mockStringTranslationService);
    \Drupal::setContainer($container);

    $mockEntity = $this->getMockBuilder('Drupal\Core\Entity\ContentEntityInterface')
      ->getMock();
    $mockEntity->expects($this->any())
      ->method('label')
      ->willReturn('My entity');

    $instance = new CleanupDeleteForm();
    $instance->setEntity($mockEntity);

    $result = $instance->getQuestion();

    $this->assertInstanceOf('\Drupal\Core\StringTranslation\TranslatableMarkup', $result);
  }

  /**
   * The delete form getDescription hook returns a translatable message.
   *
   * @test
   */
  public function cleanupDeleteFormDescriptionIsTranslatable() {

    $mockStringTranslationService = $this->getMockBuilder('Drupal\Core\StringTranslation\TranslationInterface')
      ->disableOriginalConstructor()
      ->getMock();

    $container = $this->getMockBuilder('\Symfony\Component\DependencyInjection\ContainerInterface')
      ->getMock();
    $container->expects($this->any())
      ->method('get')
      ->with('string_translation')
      ->willReturn($mockStringTranslationService);
    \Drupal::setContainer($container);

    $mockEntity = $this->getMockBuilder('Drupal\Core\Entity\ContentEntityInterface')
      ->getMock();
    $mockEntity->expects($this->any())
      ->method('label')
      ->willReturn('My entity');

    $instance = new CleanupDeleteForm();
    $instance->setEntity($mockEntity);

    $result = $instance->getDescription();

    $this->assertInstanceOf('\Drupal\Core\StringTranslation\TranslatableMarkup', $result);
  }

}
