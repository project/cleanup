<?php

namespace Drupal\Tests\cleanup\Unit;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Class MockEntitySorter.
 *
 * A mock entity type implementation that just provides a sorter for the
 * ListBuilder unit tests.
 *
 * @package Drupal\Tests\cleanup\Unit
 */
class MockEntitySorter {

  /**
   * A simple sort comparator.
   */
  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    \Drupal::state()->set('config_entity_sort', TRUE);
    return parent::sort($a, $b);
  }

}
