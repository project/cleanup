<?php

namespace Drupal\Tests\cleanup\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\cleanup\Entity\Cleanup;

/**
 * @coversDefaultClass \Drupal\cleanup\Entity\Cleanup
 *
 * @group Cleanup
 */
class CleanupTest extends UnitTestCase {

  /**
   * The entity type used for testing.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $entityType;

  /**
   * The entity manager used for testing.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $entityManager;

  /**
   * The ID of the type of the entity under test.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The mock CleanupTaskManager.
   *
   * @var \Drupal\cleanup\CleanupTaskManager
   */
  protected $cleanupTaskManager;

  /**
   * The mock CleanupTaskPluginCollection.
   *
   * @var \Drupal\cleanup\CleanupTaskPluginCollection
   */
  protected $cleanupTaskPluginCollection;

  /**
   * Plugin definitions status for the mock CleanupTaskPluginCollection.
   *
   * @var array
   */
  protected $cleanupTaskPluginCollectionDefinitions = [];

  /**
   * Plugin instances status for the mock CleanupTaskPluginCollection.
   *
   * @var array
   */
  protected $cleanupTaskPluginCollectionInstances = [];

  /**
   * Fake 'generated' UUID.
   *
   * @var string
   */
  protected $fakeGeneratedUuid;

  /**
   * Give task manager an 'instance' to return from createInstance().
   *
   * @param string $task_id
   *   The task ID for which the instance will be 'created'.
   * @param \PHPUnit_Framework_MockObject_MockObject $task
   *   The task to be returned.
   */
  protected function setTaskManagerCreateInstance($task_id, \PHPUnit_Framework_MockObject_MockObject $task) {
    $this->cleanupTaskManager->expects($this->any())
      ->method('createInstance')
      ->with($task_id)
      ->will($this->returnValue($task));

    // Add to the list for iteration.
    $this->cleanupTaskPluginCollectionInstances[$task_id] = $task;
  }

  /**
   * Gets a mocked cleanup task list for testing.
   *
   * @param array $values
   *   The array of values for the Cleanup constructor's first argument.
   * @param array $stubs
   *   The list of stubs that will be used.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   The mocked cleanup task list.
   */
  protected function getCleanupMock(array $values, array $stubs = []) {
    $default_stubs = [
      'getCleanupTaskPluginManager',
      'uuidGenerator',
    ];
    $cleanup = $this->getMockBuilder('\Drupal\cleanup\Entity\Cleanup')
      ->setConstructorArgs([$values, $this->entityTypeId])
      ->setMethods(array_merge($default_stubs, $stubs))
      ->getMock();

    $cleanup->expects($this->any())
      ->method('getCleanupTaskPluginManager')
      ->will($this->returnValue($this->cleanupTaskManager));

    $fake_uuidGenerator = $this->getMockBuilder('\Drupal\Component\Uuid\UuidInterface')
      ->getMock();
    $fake_uuidGenerator->expects($this->any())
      ->method('generate')
      ->willReturnCallback(function () {
        return $this->fake_generated_uuid ?: $this->randomMachineName();
      });

    $cleanup->expects($this->any())
      ->method('uuidGenerator')
      ->will($this->returnValue($fake_uuidGenerator));

    return $cleanup;
  }

  /**
   * Get a mock task with the provided attributes.
   *
   * @param string $id
   *   The id of the task.
   * @param string $uuid
   *   The UUID of the task.
   *
   * @return \PHPUnit_Framework_MockObject_MockObject
   *   The mock task generated.
   */
  protected function getMockTask($id, $uuid) {
    $task = $this->getMockBuilder('\Drupal\cleanup\CleanupTaskInterface')
      ->getMock();

    $task->expects($this->any())
      ->method('getPluginId')
      ->willReturn($id);

    $task->expects($this->any())
      ->method('getUuid')
      ->willReturn($uuid);

    return $task;
  }

  /**
   * Get a mock task configuration array with the provided attributes.
   *
   * @param string $id
   *   The id of the task.
   * @param string $uuid
   *   The UUID of the task.
   *
   * @return array
   *   The mock task configuration generated.
   */
  protected function getMockTaskConfigurationArray($id, $uuid) {
    return [$uuid => ['uuid' => $uuid, 'id' => $id]];
  }

  /**
   * Get a mock task configuration collection.
   *
   * @return array
   *   The mock task configuration generated.
   */
  protected function getMockTaskConfigurationCollection() {
    $this->cleanupTaskPluginCollection = $this->getMockBuilder('\Drupal\cleanup\CleanupTaskPluginCollection')
      ->disableOriginalConstructor()
      ->getMock();

    $this->cleanupTaskPluginCollection
      ->expects($this->any())
      ->method('getIterator')
      ->willReturnCallback(function () {
        return new \ArrayIterator($this->cleanupTaskPluginCollectionInstances);
      });

    $this->cleanupTaskPluginCollection
      ->expects($this->any())
      ->method('addInstanceId')
      ->willReturnCallback(function ($uuid, $configuration) {
        $this->cleanupTaskPluginCollectionDefinitions[$uuid] = $configuration;
      });

    return $this->cleanupTaskPluginCollection;
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->entityTypeId = $this->randomMachineName();
    $this->provider = $this->randomMachineName();
    $this->entityType = $this->getMockBuilder('\Drupal\Core\Entity\EntityTypeInterface')
      ->getMock();
    $this->entityType->expects($this->any())
      ->method('getProvider')
      ->will($this->returnValue($this->provider));
    $this->entityManager = $this->getMockBuilder('\Drupal\Core\Entity\EntityManagerInterface')
      ->getMock();
    $this->entityManager->expects($this->any())
      ->method('getDefinition')
      ->with($this->entityTypeId)
      ->will($this->returnValue($this->entityType));

    $this->getMockTaskConfigurationCollection();

    $this->cleanupTaskManager = $this->getMockBuilder('\Drupal\cleanup\CleanupTaskManager')
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * Id returns the machine name of the cleanup.
   *
   * @test
   */
  public function idReturnsMachineNameOfTaskList() {
    $cleanup_id = $this->randomMachineName();
    $cleanEntity = $this->getCleanupMock(['name' => $cleanup_id], []);
    $this->assertEquals($cleanup_id, $cleanEntity->id());
  }

  /**
   * Can get a list of tasks from the cleanup.
   *
   * The result of getTasks is a Collection, so check that it has the expected
   * type.
   *
   * @test
   */
  public function getTasksReturnsPluginCollection() {

    $cleanEntity = $this->getCleanupMock([], []);
    $tasks = $cleanEntity->getTasks();
    $this->assertInstanceOf('Drupal\Component\Plugin\LazyPluginCollection', $tasks);
  }

  /**
   * GetPluginCollections wraps tasks collection in an array.
   *
   * @test
   */
  public function getPluginCollectionsReturnsTasksCollectionWrappedInAnArray() {

    $cleanEntity = $this->getCleanupMock([], []);
    $result = $cleanEntity->getPluginCollections();
    $this->assertArrayHasKey('tasks', $result);
    $this->assertInstanceOf('Drupal\Component\Plugin\LazyPluginCollection', $result['tasks']);

    unset($result['tasks']);
    $this->assertEmpty($result);
  }

  /**
   * Can get a specific task by id.
   *
   * @test
   */
  public function getTaskReturnsTheRequestedInstance() {
    $task_id = $this->randomMachineName();
    $uuid = $this->randomMachineName();

    $task = $this->getMockTask($task_id, $uuid);
    $this->setTaskManagerCreateInstance($task_id, $task);

    $definitions = [
      'tasks' => $this->getMockTaskConfigurationArray($task_id, $uuid),
    ];

    $cleanEntity = $this->getCleanupMock($definitions, []);
    $actual = $cleanEntity->getTask($uuid);
    $this->assertEquals($task, $actual);
  }

  /**
   * Can add a task to the list.
   *
   * We're not testing the actual addition in the base class -
   * just that we invoke the method with the expected params.
   *
   * @test
   */
  public function canAddTaskToTheList() {
    $task_id = $this->randomMachineName();
    $uuid = $this->fake_generated_uuid = $this->randomMachineName();

    $task = $this->getMockTask($task_id, $uuid);
    $this->setTaskManagerCreateInstance($task_id, $task);

    $cleanEntity = $this->getCleanupMock([], []);

    $uuid_result = $cleanEntity->addCleanupTask(['id' => $task_id]);
    $this->assertEquals($uuid, $uuid_result);

    // getTask uses the UUID.
    $actual = $cleanEntity->getTask($uuid);
    $this->assertEquals($task, $actual);

  }

  /**
   * Can delete a task from the list.
   *
   * Again, just checking invocation of the base class.
   *
   * @test
   */
  public function canRemoveTaskFromTheList() {
    $task_id = $this->randomMachineName();
    $uuid = $this->fake_generated_uuid = $this->randomMachineName();

    $task = $this->getMockTask($task_id, $uuid);
    $this->setTaskManagerCreateInstance($task_id, $task);

    $definitions = [
      'tasks' => $this->getMockTaskConfigurationArray($task_id, $uuid),
    ];

    $cleanEntity = $this->getCleanupMock($definitions, ['save']);
    $cleanEntity->expects($this->once())
      ->method('save');

    $cleanEntity->deleteCleanupTask($task);

    $this->assertEmpty($cleanEntity->getTasks());
  }

  /**
   * Cleanup Task Plugin Manager uses service discovery.
   *
   * @test
   */
  public function getCleanupTaskPluginManagerUsesServiceDiscovery() {
    $container = $this->getMockBuilder('Drupal\Core\DependencyInjection\Container')
      ->getMock();
    $container->expects($this->once())
      ->method('get')
      ->with('plugin.manager.cleanup_task');

    \Drupal::setContainer($container);

    $reflection_class = new \ReflectionClass('Drupal\cleanup\Entity\Cleanup');
    $method = $reflection_class->getMethod('getCleanupTaskPluginManager');
    $method->setAccessible(TRUE);

    $instance = new Cleanup([], []);

    $method->invoke($instance);
  }

  /**
   * Calls runCleanup on each task attached when runCleanup invoked.
   *
   * @test
   */
  public function runCleanupInvokesRunCleanupForEachTask() {
    $task_id = $this->randomMachineName();
    $uuid = $this->fake_generated_uuid = $this->randomMachineName();

    $task = $this->getMockTask($task_id, $uuid);
    $this->setTaskManagerCreateInstance($task_id, $task);

    $cleanEntity = $this->getCleanupMock([], []);

    $cleanEntity->addCleanupTask(['id' => $task_id]);

    $task->expects($this->once())
      ->method('runCleanup');

    $cleanEntity->runCleanup();
  }

}
