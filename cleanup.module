<?php

/**
 * @file
 * Exposes global functionality for creating cleanup task lists.
 */

use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function cleanup_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.cleanup':
      $cleanup_url = Url::fromRoute('entity.cleanup.collection')->toString();
      $output = "<h3>" . t("About") . "</h3>";
      $output .= "<p>" . t("The Cleanup module provides a way to quickly and easily make modifications to a copy of your production database, removing or modifying sensitive data, API keys, or other configuration.") . "</p>";
      $output .= "<h3>" . t("Configuring") . "</h3>";
      $output .= "<p>" . t("The cleanup module is uses the concept of 'tasks' - individuals modifications or groups of modifications that can be made to a database. Tasks are defined by plugins provided by the Cleanup module itself, or other modules and each perform a single task or related set of tasks, such as clearing a log or modifying an API key.") . "</p>";
      $output .= "<p>" . t("To make use of tasks, you first visit the <a href=':cleanup_url'>administration page</a>, and create a 'cleanup', which can be thought of as a list to which these tasks can be added.", [':cleanup_url' => $cleanup_url]) . "</p>";
      $output .= "<p>" . t("Having created a cleanup, you add tasks to it by selecting them from the list of unused tasks in the 'Select a task' dropdown near the bottom of the page, and click the 'Add' button.") . "</p>";
      $output .= "<p>" . t("If the task is configurable, you will then be taken to its configuration form. If it is not configurable, you will be returned to the page you were on, with the new task added to the list.") . "</p>";
      $output .= "<p>" . t("Tasks may be configured or removed via the operations menu on the right side of the cleanup configuration page.") . "</p>";
      $output .= "<p>" . t("Cleanups may be removed or updated via the operations menu in page linked above.") . "</p>";
      $output .= "<h3>" . t("Execution") . "</h3>";
      $output .= "<p>" . t("To run a cleanup, visit the operations menu in page linked above or <i>drush clean < cleanup machine name ></i>. <i>drush clean</i> without the name of a configuration will return the names of current configurations.") . "</p>";
      return $output;

    case 'entity.cleanup.collection':
      return '<p>' . t('Cleanup configurations allow you to configure a series of tasks to run via the UI or drush, to perform tasks like preparing a copy of a production database for use in development or training.') . '</p>';

    case 'cleanup.task_add_form':
      $task = \Drupal::service('plugin.manager.cleanup_task')->getDefinition($route_match->getParameter('cleanup_task'));
      return isset($task['description']) ? ('<p>' . $task['description'] . '</p>') : NULL;

    case 'cleanup.task_edit_form':
      $task = $route_match->getParameter('cleanup')->getTask($route_match->getParameter('cleanup_task'));
      $task_definition = $task->getPluginDefinition();
      return isset($task_definition['description']) ? ('<p>' . $task_definition['description'] . '</p>') : NULL;
  }
}
