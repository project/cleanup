<?php

namespace Drupal\cleanup;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages cleanup task plugins.
 *
 * @see hook_cleanup_task_info_alter()
 * @see \Drupal\cleanup\Annotation\CleanupTask
 * @see \Drupal\cleanup\ConfigurableCleanupTaskInterface
 * @see \Drupal\cleanup\ConfigurableCleanupTaskBase
 * @see \Drupal\cleanup\CleanupTaskInterface
 * @see \Drupal\cleanup\CleanupTaskBase
 * @see plugin_api
 */
class CleanupTaskManager extends DefaultPluginManager {

  /**
   * Constructs a new CleanupTaskManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/CleanupTask', $namespaces, $module_handler, 'Drupal\cleanup\CleanupTaskInterface', 'Drupal\cleanup\Annotation\CleanupTask');

    $this->alterInfo('cleanup_task_info');
    $this->setCacheBackend($cache_backend, 'cleanup_task_plugins');
  }

  /**
   * Constructs a new task instance.
   */
  public function getTask($task_id) {
    if (empty($this->definitions[$task_id])) {
      return NULL;
    }

    return $this->createInstance($task_id, []);
  }

}
