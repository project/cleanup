<?php

namespace Drupal\cleanup\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\cleanup\ConfigurableCleanupTaskInterface;
use Drupal\cleanup\CleanupInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base form for cleanup tasks.
 */
abstract class CleanupTaskFormBase extends FormBase {

  /**
   * The cleanup task list.
   *
   * @var \Drupal\cleanup\CleanupInterface
   */
  protected $cleanup;

  /**
   * The cleanup task.
   *
   * @var \Drupal\cleanup\CleanupTaskInterface|\Drupal\cleanup\ConfigurableCleanupTaskInterface
   */
  protected $cleanupTask;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cleanup_task_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CleanupInterface $cleanup = NULL, $task = NULL) {
    $this->cleanup = $cleanup;
    try {
      $this->cleanupTask = $this->prepareCleanupTask($task);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException("Invalid task id: '$task'.");
    }
    $request = $this->getRequest();

    if (!($this->cleanupTask instanceof ConfigurableCleanupTaskInterface)) {
      throw new NotFoundHttpException();
    }

    $form['#attached']['library'][] = 'image/admin';
    $form['uuid'] = [
      '#type' => 'value',
      '#value' => $this->cleanupTask->getUuid(),
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->cleanupTask->getPluginId(),
    ];

    $form['data'] = [];
    $subform_state = SubformState::createForSubform($form['data'], $form, $form_state);
    $form['data'] = $this->cleanupTask->buildConfigurationForm($form['data'], $subform_state);
    $form['data']['#tree'] = TRUE;

    // Check the URL for a weight, then the cleanup_task, otherwise use default.
    $form['weight'] = [
      '#type' => 'hidden',
      '#value' => $request->query->has('weight') ? (int) $request->query->get('weight') : $this->cleanupTask->getWeight(),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->cleanup->toUrl('edit-form'),
      '#attributes' => ['class' => ['button']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // The cleanup task configuration is stored in the 'data' key in the form,
    // pass that through for validation.
    $this->cleanupTask->validateConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    // The cleanup_task configuration is stored in the 'data' key in the form,
    // pass that through for submission.
    $this->cleanupTask->submitConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));

    $this->cleanupTask->setWeight($form_state->getValue('weight'));
    if (!$this->cleanupTask->getUuid()) {
      $this->cleanup->addCleanupTask($this->cleanupTask->getConfiguration());
    }
    $this->cleanup->save();

    $this->messenger()->addStatus($this->t('The cleanup task was successfully saved.'));
    $form_state->setRedirectUrl($this->cleanup->toUrl('edit-form'));
  }

  /**
   * Converts an cleanup_task ID into an object.
   *
   * @param string $cleanup_task
   *   The cleanup_task ID.
   *
   * @return \Drupal\cleanup\CleanupTaskInterface
   *   The cleanup_task object.
   */
  abstract protected function prepareCleanupTask($cleanup_task);

}
