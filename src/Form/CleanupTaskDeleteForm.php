<?php

namespace Drupal\cleanup\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cleanup\CleanupInterface;

/**
 * Form for deleting an cleanup task.
 *
 * @internal
 */
class CleanupTaskDeleteForm extends ConfirmFormBase {

  /**
   * The cleanup task list containing the cleanup task to be deleted.
   *
   * @var \Drupal\cleanup\CleanupInterface
   */
  protected $cleanup;

  /**
   * The cleanup task to be deleted.
   *
   * @var \Drupal\cleanup\CleanupTaskInterface
   */
  protected $cleanupTask;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the @task task from the %task_list task list?', ['%task_list' => $this->cleanup->label(), '@task' => $this->cleanupTask->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->cleanup->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cleanup_task_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CleanupInterface $cleanup = NULL, $cleanup_task = NULL) {
    $this->cleanup = $cleanup;
    $this->cleanupTask = $this->cleanup->getTask($cleanup_task);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->cleanup->deleteCleanupTask($this->cleanupTask);
    $this->messenger()->addStatus($this->t('The cleanup task %name has been deleted.', ['%name' => $this->cleanupTask->label()]));
    $form_state->setRedirectUrl($this->cleanup->toUrl('edit-form'));
  }

}
