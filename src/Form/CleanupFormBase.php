<?php

namespace Drupal\cleanup\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form for cleanup task list add and edit forms.
 */
abstract class CleanupFormBase extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\cleanup\CleanupInterface
   */
  protected $entity;

  /**
   * The cleanup task list entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $cleanupStorage;

  /**
   * Constructs a base class for cleanup task list add and edit forms.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $cleanup_storage
   *   The cleanup task list entity storage.
   */
  public function __construct(EntityStorageInterface $cleanup_storage) {
    $this->cleanupStorage = $cleanup_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('cleanup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cleanup task list name'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];
    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this->cleanupStorage, 'load'],
      ],
      '#default_value' => $this->entity->id(),
      '#required' => TRUE,
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
  }

}
