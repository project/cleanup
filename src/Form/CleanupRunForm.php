<?php

namespace Drupal\cleanup\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cleanup\CleanupInterface;
use Drupal\Core\Url;

/**
 * Confirmation form for running a task list.
 *
 * @internal
 */
class CleanupRunForm extends EntityConfirmFormBase {

  /**
   * The cleanup task list to be run.
   *
   * @var \Drupal\cleanup\CleanupInterface
   */
  protected $cleanup;

  /**
   * Gets the configuration manager.
   *
   * @return \Drupal\Core\Config\ConfigManager
   *   The configuration manager.
   */
  protected function getConfigManager() {
    return \Drupal::service('config.manager');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to run the %task_list cleanup?', ['%task_list' => $this->cleanup->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Run');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.cleanup.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cleanup_task_run_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CleanupInterface $cleanup = NULL) {
    $this->cleanup = $cleanup;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->cleanup->runCleanup();
    $this->messenger()->addStatus($this->t('The %name cleanup has completed.', ['%name' => $this->cleanup->label()]));
    $form_state->setRedirectUrl(new Url('entity.cleanup.collection'));
  }

}
