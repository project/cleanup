<?php

namespace Drupal\cleanup\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\cleanup\CleanupTaskManager;
use Drupal\cleanup\CleanupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add form for cleanup tasks.
 *
 * @internal
 */
class CleanupTaskAddForm extends CleanupTaskFormBase {

  /**
   * The cleanup_task manager.
   *
   * @var \Drupal\cleanup\CleanupTaskManager
   */
  protected $taskManager;

  /**
   * Constructs a new CleanupTaskAddForm.
   *
   * @param \Drupal\cleanup\CleanupTaskManager $task_manager
   *   The cleanup_task manager.
   */
  public function __construct(CleanupTaskManager $task_manager) {
    $this->taskManager = $task_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.cleanup_task')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CleanupInterface $cleanup = NULL, $cleanup_task = NULL) {
    $form = parent::buildForm($form, $form_state, $cleanup, $cleanup_task);

    $form['#title'] = $this->t('Add %label task', ['%label' => $this->cleanupTask->label()]);
    $form['actions']['submit']['#value'] = $this->t('Add task');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareCleanupTask($cleanup_task) {
    $cleanup_task = $this->taskManager->createInstance($cleanup_task);
    // Set the initial weight so this task comes last.
    $cleanup_task->setWeight(count($this->cleanup->getTasks()));
    return $cleanup_task;
  }

}
