<?php

namespace Drupal\cleanup\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\cleanup\CleanupInterface;

/**
 * Provides an edit form for cleanup tasks.
 *
 * @internal
 */
class CleanupTaskEditForm extends CleanupTaskFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CleanupInterface $cleanup = NULL, $cleanup_task = NULL) {
    $form = parent::buildForm($form, $form_state, $cleanup, $cleanup_task);

    $form['#title'] = $this->t('Edit %label task', ['%label' => $this->cleanupTask->label()]);
    $form['actions']['submit']['#value'] = $this->t('Update task');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareCleanupTask($cleanup_task) {
    return $this->cleanup->getTask($cleanup_task);
  }

}
