<?php

namespace Drupal\cleanup\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\cleanup\CleanupTaskManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for cleanup task list edit form.
 *
 * @internal
 */
class CleanupEditForm extends CleanupFormBase {

  /**
   * The cleanup task manager service.
   *
   * @var \Drupal\cleanup\CleanupTaskManager
   */
  protected $cleanupTaskManager;

  /**
   * Constructs an CleanupEditForm object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $cleanup_storage
   *   The storage.
   * @param \Drupal\cleanup\CleanupTaskManager $cleanup_task_manager
   *   The cleanup task manager service.
   */
  public function __construct(EntityStorageInterface $cleanup_storage, CleanupTaskManager $cleanup_task_manager) {
    parent::__construct($cleanup_storage);
    $this->cleanupTaskManager = $cleanup_task_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('cleanup'),
      $container->get('plugin.manager.cleanup_task')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $form['#title'] = $this->t('Edit task list %name', ['%name' => $this->entity->label()]);
    $form['#tree'] = TRUE;

    // Build the list of existing cleanup tasks for this cleanup task list.
    $form['tasks'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Task'),
        $this->t('Weight'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'cleanup-task-order-weight',
        ],
      ],
      '#attributes' => [
        'id' => 'cleanup-tasklist-tasks',
      ],
      '#empty' => t('There are currently no tasks in this task list. Add one by selecting an option below.'),
      // Render tasks below parent elements.
      '#weight' => 5,
    ];
    foreach ($this->entity->getTasks() as $task) {
      $key = $task->getUuid();
      $form['tasks'][$key]['#attributes']['class'][] = 'draggable';
      $form['tasks'][$key]['#weight'] = isset($user_input['tasks']) ? $user_input['tasks'][$key]['weight'] : NULL;
      $form['tasks'][$key]['task'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $task->label(),
          ],
        ],
      ];

      $summary = $task->getSummary();

      if (!empty($summary)) {
        $summary['#prefix'] = ' ';
        $form['tasks'][$key]['task']['data']['summary'] = $summary;
      }

      $form['tasks'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $task->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $task->getWeight(),
        '#attributes' => [
          'class' => ['cleanup-task-order-weight'],
        ],
      ];

      $links = [];
      $is_configurable = ($task->hasConfigurationForm());
      if ($is_configurable) {
        $links['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('cleanup.task_edit_form', [
            'cleanup' => $this->entity->id(),
            'cleanup_task' => $key,
          ]),
        ];
      }
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('cleanup.task_delete', [
          'cleanup' => $this->entity->id(),
          'cleanup_task' => $key,
        ]),
      ];
      $form['tasks'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
    }

    // Build the new cleanup task addition form and add it to the task list.
    $new_task_options = [];
    $tasks = $this->cleanupTaskManager->getDefinitions();
    foreach ($this->entity->getTasks() as $task) {
      unset($tasks[$task->getPluginId()]);
    }

    if (!empty($tasks)) {
      uasort($tasks, function ($a, $b) {
        return Unicode::strcasecmp($a['label'], $b['label']);
      });
      foreach ($tasks as $task => $definition) {
        $new_task_options[$task] = $definition['label'];
      }
      $form['tasks']['new'] = [
        '#tree' => FALSE,
        '#weight' => isset($user_input['weight']) ? $user_input['weight'] : NULL,
        '#attributes' => ['class' => ['draggable']],
      ];
      $form['tasks']['new']['task'] = [
        'data' => [
          'new' => [
            '#type' => 'select',
            '#title' => $this->t('Effect'),
            '#title_display' => 'invisible',
            '#options' => $new_task_options,
            '#empty_option' => $this->t('Select a new task'),
          ],
          [
            'add' => [
              '#type' => 'submit',
              '#value' => $this->t('Add'),
              '#validate' => ['::taskValidate'],
              '#submit' => ['::submitForm', '::taskSave'],
            ],
          ],
        ],
        '#prefix' => '<div class="cleanup.tasklist-new">',
        '#suffix' => '</div>',
      ];

      $form['tasks']['new']['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for new task'),
        '#title_display' => 'invisible',
        '#default_value' => count($this->entity->getTasks()) + 1,
        '#attributes' => ['class' => ['cleanup.task-order-weight']],
      ];
      $form['tasks']['new']['operations'] = [
        'data' => [],
      ];
    }

    return parent::form($form, $form_state);
  }

  /**
   * Validate handler for cleanup task.
   */
  public function taskValidate($form, FormStateInterface $form_state) {
    if (!$form_state->getValue('new')) {
      $form_state->setErrorByName('new', $this->t('Select an task to add.'));
    }
  }

  /**
   * Submit handler for cleanup task.
   */
  public function taskSave($form, FormStateInterface $form_state) {
    $this->save($form, $form_state);

    // Check whether this field has any configuration options.
    $task = $this->cleanupTaskManager->getDefinition($form_state->getValue('new'));
    $instance = $this->cleanupTaskManager->createInstance($task['id']);

    // Load the configuration form for this option.
    if ($instance->hasConfigurationForm()) {
      $form_state->setRedirect(
        'cleanup.task_add_form',
        [
          'cleanup' => $this->entity->id(),
          'cleanup_task' => $form_state->getValue('new'),
        ],
        ['query' => ['weight' => $form_state->getValue('weight')]]
      );
    }
    // If there's no form, immediately add the cleanup task.
    else {
      $task = [
        'id' => $task['id'],
        'data' => [],
        'weight' => $form_state->getValue('weight'),
      ];
      $task_id = $this->entity->addCleanupTask($task);
      $this->entity->save();
      if (!empty($task_id)) {
        $this->messenger()
          ->addStatus($this->t('The task was successfully added.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Update cleanup task weights.
    if (!$form_state->isValueEmpty('tasks')) {
      $this->updateEffectWeights($form_state->getValue('tasks'));
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Updates cleanup task weights.
   *
   * @param array $tasks
   *   Associative array with tasks having task uuid as keys and array
   *   with task data as values.
   */
  protected function updateEffectWeights(array $tasks) {
    foreach ($tasks as $uuid => $task_data) {
      if ($this->entity->getTasks()->has($uuid)) {
        $this->entity->getTask($uuid)->setWeight($task_data['weight']);
      }
    }
  }

}
