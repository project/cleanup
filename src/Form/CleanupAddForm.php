<?php

namespace Drupal\cleanup\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Controller for cleanup task list addition forms.
 *
 * @internal
 */
class CleanupAddForm extends CleanupFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Cleanup "%name" was created.', ['%name' => $this->entity->label()]));
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Create new cleanup task list');

    return $actions;
  }

}
