<?php

namespace Drupal\cleanup\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Creates a form to delete an cleanup task list.
 *
 * @internal
 */
class CleanupDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Optionally select a task list before deleting %task_list', ['%task_list' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Remove this cleanup task list.');
  }

}
