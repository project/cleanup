<?php

namespace Drupal\cleanup\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\cleanup\CleanupTaskPluginCollection;
use Drupal\cleanup\CleanupTaskInterface;
use Drupal\cleanup\CleanupInterface;

/**
 * Defines an cleanup task list configuration entity.
 *
 * @ConfigEntityType(
 *   id = "cleanup",
 *   label = @Translation("Cleanup Task List"),
 *   label_collection = @Translation("Cleanup task lists"),
 *   label_singular = @Translation("cleanup task list"),
 *   label_plural = @Translation("cleanup task lists"),
 *   label_count = @PluralTranslation(
 *     singular = "@count cleanup task list",
 *     plural = "@count cleanup task lists",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\cleanup\Form\CleanupAddForm",
 *       "edit" = "Drupal\cleanup\Form\CleanupEditForm",
 *       "delete" = "Drupal\cleanup\Form\CleanupDeleteForm",
 *       "run" = "Drupal\cleanup\Form\CleanupRunForm"
 *     },
 *     "list_builder" = "Drupal\cleanup\CleanupListBuilder",
 *     "storage" = "Drupal\cleanup\CleanupStorage",
 *   },
 *   admin_permission = "administer cleanup task lists",
 *   config_prefix = "task_list",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label"
 *   },
 *   links = {
 *     "run" = "/admin/config/cleanup/manage/{cleanup}/run",
 *     "flush-form" = "/admin/config/cleanup/manage/{cleanup}/flush",
 *     "edit-form" = "/admin/config/cleanup/manage/{cleanup}",
 *     "delete-form" = "/admin/config/cleanup/manage/{cleanup}/delete",
 *     "collection" = "/admin/config/cleanup",
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "tasks",
 *   }
 * )
 */
class Cleanup extends ConfigEntityBase implements CleanupInterface, EntityWithPluginCollectionInterface {

  /**
   * The name of the cleanup task list.
   *
   * @var string
   */
  protected $name;

  /**
   * The cleanup task list label.
   *
   * @var string
   */
  protected $label;

  /**
   * The array of tasks for this cleanup.
   *
   * @var array
   */
  protected $tasks = [];

  /**
   * Holds the collection of tasks that are used by this cleanup.
   *
   * @var \Drupal\cleanup\CleanupTaskPluginCollection
   */
  protected $tasksCollection;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCleanupTask(CleanupTaskInterface $task) {
    $this->getTasks()->removeInstanceId($task->getUuid());
    $this->save();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTask($task) {
    return $this->getTasks()->get($task);
  }

  /**
   * {@inheritdoc}
   */
  public function getTasks() {
    if (!$this->tasksCollection) {
      $this->tasksCollection = new CleanupTaskPluginCollection($this->getCleanupTaskPluginManager(), $this->tasks);
      $this->tasksCollection->sort();
    }
    return $this->tasksCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['tasks' => $this->getTasks()];
  }

  /**
   * {@inheritdoc}
   */
  public function addCleanupTask(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getTasks()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  /**
   * Returns the cleanup_task plugin manager.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   The cleanup_task plugin manager.
   */
  protected function getCleanupTaskPluginManager() {
    return \Drupal::service('plugin.manager.cleanup_task');
  }

  /**
   * {@inheritdoc}
   */
  public function runCleanup() {
    $tasks = $this->getTasks();

    foreach ($tasks as $task) {
      $task->runCleanup();
    }

  }

}
