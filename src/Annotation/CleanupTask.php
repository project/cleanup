<?php

namespace Drupal\cleanup\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an cleanup_task annotation object.
 *
 * Plugin Namespace: Plugin\CleanupTask.
 *
 * @Annotation
 */
class CleanupTask extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the cleanup_task.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the cleanup_task (optional).
   *
   * This will be shown when adding or configuring this cleanup_task.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

}
