<?php

namespace Drupal\cleanup;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an cleanup task list entity.
 */
interface CleanupInterface extends ConfigEntityInterface {

  /**
   * Runs the list of tasks.
   *
   * @return $this
   */
  public function runCleanup();

  /**
   * Returns a specific cleanup task.
   *
   * @param string $task
   *   The cleanup task ID.
   *
   * @return \Drupal\cleanup\CleanupTaskInterface
   *   The cleanup task object.
   */
  public function getTask($task);

  /**
   * Returns the cleanup tasks for this task list.
   *
   * @return \Drupal\cleanup\CleanupTaskPluginCollection|\Drupal\cleanup\CleanupTaskInterface[]
   *   The cleanup task plugin collection.
   */
  public function getTasks();

  /**
   * Saves an cleanup task for this task list.
   *
   * @param array $configuration
   *   An array of cleanup task configuration.
   *
   * @return string
   *   The cleanup task ID.
   */
  public function addCleanupTask(array $configuration);

  /**
   * Deletes an cleanup task from this task list.
   *
   * @param \Drupal\cleanup\CleanupTaskInterface $task
   *   The cleanup task object.
   *
   * @return $this
   */
  public function deleteCleanupTask(CleanupTaskInterface $task);

}
