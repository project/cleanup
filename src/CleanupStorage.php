<?php

namespace Drupal\cleanup;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Storage controller class for "cleanup task list" configuration entities.
 */
class CleanupStorage extends ConfigEntityStorage implements CleanupStorageInterface {}
