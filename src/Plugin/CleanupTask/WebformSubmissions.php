<?php

namespace Drupal\cleanup\Plugin\CleanupTask;

use Drupal\cleanup\CleanupTaskInterface;
use Drupal\cleanup\ConfigurableCleanupTaskBase;

/**
 * Removes any webform submissions that have been saved.
 *
 * @CleanupTask(
 *   id = "webform_submissions",
 *   label = @Translation("Purge webform submissions"),
 *   description = @Translation("Removes any webform submissions that have been saved."),
 * )
 */
class WebformSubmissions extends ConfigurableCleanupTaskBase implements CleanupTaskInterface {

  /**
   * {@inheritdoc}
   */
  public function runCleanup() {
    $logger = $this->container->get('messenger');
    $database = $this->container->get('database');

    if (!$database->schema()->tableExists('webform_submission')) {
      $logger->addMessage($this->t('No webform submission table found - nothing to be cleaned.'));
      return TRUE;
    }

    $database->truncate('webform_submission')->execute();
    $database->truncate('webform_submission_data')->execute();
    $database->truncate('webform_submission_log')->execute();
    $logger->addMessage($this->t('Webform submissions removed.'));
    return TRUE;
  }

}
