<?php

namespace Drupal\cleanup\Plugin\CleanupTask;

use Drupal\cleanup\CleanupTaskInterface;
use Drupal\cleanup\ConfigurableCleanupTaskBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Site meta data cleaner.
 *
 * @CleanupTask(
 *   id = "site_metadata",
 *   label = @Translation("Modify or clear site metadata"),
 *   description = @Translation("Clean site metadata."),
 * )
 */
class SiteMetadata extends ConfigurableCleanupTaskBase implements CleanupTaskInterface {

  /**
   * Details of the settings that can be removed / modified.
   *
   * @var array
   */
  static protected $settings = [
    'site_name' => [
      'title' => 'Site Name',
      'description' => 'Enter the replacement site name',
      'editable' => 'system.site',
      'key' => 'name',
    ],
    'site_slogan' => [
      'title' => 'Site Slogan',
      'description' => 'Enter the replacement site slogan',
      'editable' => 'system.site',
      'key' => 'slogan',
    ],
    'site_mail' => [
      'title' => 'Site Email',
      'description' => 'Enter the replacement site email address',
      'editable' => 'system.site',
      'key' => 'mail',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() +
      [
        'use_replacement_keys' => TRUE,
        'replacements' => [
          'site_name' => '',
          'site_slogan' => '',
          'site_mail' => '',
        ],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {

    $summary = [
      '#type' => 'container',
      '#tree' => TRUE,
      'header' => [
        '#type' => 'markup',
        '#markup' => '<p>Replacing settings with the following values:</p>',
      ],
      'list' => [
        '#theme' => 'item_list',
        '#items' => [],
      ],
    ];

    foreach (self::$settings as $key => $info) {
      $newValueSetting = $this->configuration['replacements'][$key];
      $newValue = $this->configuration['use_replacement_keys'] && $newValueSetting ?
        $newValueSetting : 'unset';
      $summary['list']['#items'][$key] = $info['title'] . ': ' . $newValue;
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function runCleanup() {
    $logger = $this->container->get('messenger');

    foreach (self::$settings as $key => $info) {
      $newValueSetting = $this->configuration['replacements'][$key];
      $newValue = $this->configuration['use_replacement_keys'] && $newValueSetting ?
        $newValueSetting : '';

      if ($newValue) {
        $this->configFactory->getEditable($info['editable'])
          ->set($info['key'], $newValue)
          ->save();
      }
      else {
        $this->configFactory->getEditable($info['editable'])
          ->clear($info['key'])
          ->save();
      }
    }

    $logger->addMessage('Site name. slogan and email address cleaned.');

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['use_replacement_keys'] = [
      '#type' => 'checkbox',
      '#title' => 'Change the values',
      '#default_value' => $this->configuration['use_replacement_keys'],
    ];

    $form['replacements'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[id="edit-data-use-replacement-keys"]' => ['checked' => TRUE],
        ],
      ],
    ];

    foreach (self::$settings as $key => $info) {
      $form['replacements'][$key] = [
        '#type' => 'textfield',
        '#title' => $info['title'],
        '#description' => $info['description'],
        '#default_value' => $this->configuration['replacements'][$key],
      ];
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }

    $this->setConfiguration(
      [
        'uuid' => $this->getUuid(),
        'weight' => $this->getWeight(),
        'data' => array_intersect_key(
          $form_state->getValues(),
          $this->defaultConfiguration()
        ),
      ]
    );
  }

}
