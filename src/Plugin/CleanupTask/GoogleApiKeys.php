<?php

namespace Drupal\cleanup\Plugin\CleanupTask;

use Drupal\cleanup\CleanupTaskInterface;
use Drupal\cleanup\ConfigurableCleanupTaskBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Modify Google API keys.
 *
 * @CleanupTask(
 *   id = "google_api_keys",
 *   label = @Translation("Modify or clear Google API keys"),
 *   description = @Translation("Modify API keys for Google services."),
 * )
 */
class GoogleApiKeys extends ConfigurableCleanupTaskBase implements CleanupTaskInterface {

  /**
   * Details of the settings that can be removed / modified.
   *
   * @var array
   */
  static protected $settings = [
    'ga_key' => [
      'title' => 'Google Analytics Account ID',
      'description' => 'Enter the replacement Google Analytics account ID',
      'editable' => 'google_analytics.settings',
      'key' => 'account',
    ],
    'gtm_token' => [
      'title' => 'Google Tag Manager environment key',
      'description' => 'Enter the replacement Google Tag Manager environment token',
      'editable' => 'google_tag.settings',
      'key' => 'environment_token',
    ],
    'recaptcha_site_key' => [
      'title' => 'Recaptcha site key',
      'description' => 'Enter the replacement Recaptcha site key',
      'editable' => 'recaptcha.settings',
      'key' => 'site_key',
    ],
    'recaptcha_secret_key' => [
      'title' => 'Recaptcha secret key',
      'description' => 'Enter the replacement Recaptcha site secret',
      'editable' => 'recaptcha.settings',
      'key' => 'secret_key',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() +
      [
        'use_replacement_keys' => TRUE,
        'replacements' => [
          'ga_key' => '',
          'gtm_token' => '',
          'recaptcha_site_key' => '',
          'recaptcha_secret_key' => '',
        ],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {

    $summary = [
      '#type' => 'container',
      '#tree' => TRUE,
      'header' => [
        '#type' => 'markup',
        '#markup' => '<p>Replacing keys with the following values:</p>',
      ],
      'list' => [
        '#theme' => 'item_list',
        '#items' => [],
      ],
    ];

    foreach (self::$settings as $key => $info) {
      $newValueSetting = $this->configuration['replacements'][$key];
      $newValue = $this->configuration['use_replacement_keys'] && $newValueSetting ?
        $newValueSetting : 'unset';
      $summary['list']['#items'][$key] = $info['title'] . ': ' . $newValue;
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function runCleanup() {
    $logger = $this->container->get('messenger');

    foreach (self::$settings as $key => $info) {
      $newValueSetting = $this->configuration['replacements'][$key];
      $newValue = $this->configuration['use_replacement_keys'] && $newValueSetting ?
        $newValueSetting : '';

      if ($newValue) {
        $this->configFactory->getEditable($info['editable'])
          ->set($info['key'], $newValue)
          ->save();
      }
      else {
        $this->configFactory->getEditable($info['editable'])
          ->clear($info['key'])
          ->save();
      }
    }

    $logger->addMessage('API keys for Google Analytics, Google Tag Manager and Recaptcha cleaned.');

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['use_replacement_keys'] = [
      '#type' => 'checkbox',
      '#title' => 'Use different key(s).',
      '#default_value' => $this->configuration['use_replacement_keys'],
    ];

    $form['replacements'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[id="edit-data-use-replacement-keys"]' => ['checked' => TRUE],
        ],
      ],
    ];

    foreach (self::$settings as $key => $info) {
      $form['replacements'][$key] = [
        '#type' => 'textfield',
        '#title' => $info['title'],
        '#description' => $info['description'],
        '#default_value' => $this->configuration['replacements'][$key],
      ];
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }

    $this->setConfiguration(
      [
        'uuid' => $this->getUuid(),
        'weight' => $this->getWeight(),
        'data' => array_intersect_key(
          $form_state->getValues(),
          $this->defaultConfiguration()
        ),
      ]
    );
  }

}
