<?php

namespace Drupal\cleanup\Plugin\CleanupTask;

use Drupal\cleanup\CleanupTaskInterface;
use Drupal\cleanup\ConfigurableCleanupTaskBase;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Resets all user passwords.
 *
 * @CleanupTask(
 *   id = "user_passwords",
 *   label = @Translation("Randomise user passwords"),
 *   description = @Translation("Resets all user passwords."),
 * )
 */
class UserPasswords extends ConfigurableCleanupTaskBase implements CleanupTaskInterface {

  /**
   * {@inheritdoc}
   */
  public function getSummary() {

    $summary = [
      '#type' => 'container',
      '#tree' => TRUE,
      'header' => [
        '#type' => 'markup',
        '#markup' => $this->t('<p>User 1 %state</p>', [
          '%state' => $this->configuration['include_user_1'] ? 'included' : 'excluded'
        ])
      ],
      'list' => [
        '#theme' => 'item_list',
        '#items' => [],
      ],
    ];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() +
      [
        'include_user_1' => TRUE,
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['include_user_1'] = [
      '#type' => 'checkbox',
      '#title' => 'Include user 1.',
      '#default_value' => $this->configuration['include_user_1'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }

    $this->setConfiguration(
      [
        'uuid' => $this->getUuid(),
        'weight' => $this->getWeight(),
        'data' => array_intersect_key(
          $form_state->getValues(),
          $this->defaultConfiguration()
        ),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runCleanup() {
    $ids = \Drupal::entityQuery('user')
      ->execute();

    unset($ids[0]);

    if (!$this->configuration['include_user_1']) {
      unset($ids[1]);
    }

    $users = User::loadMultiple($ids);

    foreach ($users as $user) {
      $new_pass = Random::string(32);
      $user->setPassword($new_pass)->save();
    }

    $this->container->get('messenger')->addMessage(t('User passwords reset.'));
    return TRUE;
  }

}
