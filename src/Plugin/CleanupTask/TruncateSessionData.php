<?php

namespace Drupal\cleanup\Plugin\CleanupTask;

use Drupal\cleanup\CleanupTaskInterface;
use Drupal\cleanup\ConfigurableCleanupTaskBase;

/**
 * Truncates the session data table, logging all users out.
 *
 * @CleanupTask(
 *   id = "truncate_session_data",
 *   label = @Translation("Truncate session data"),
 *   description = @Translation("Truncates the session data table, logging all users out."),
 * )
 */
class TruncateSessionData extends ConfigurableCleanupTaskBase implements CleanupTaskInterface {

  /**
   * {@inheritdoc}
   */
  public function runCleanup() {
    $logger = $this->container->get('messenger');
    $database = $this->container->get('database');

    if (!$database->schema()->tableExists('webform_submission')) {
      $logger->addMessage($this->t('No webform submission table found - nothing to be cleaned.'));
      return TRUE;
    }

    $database->truncate('sessions')->execute();
    $logger->addMessage($this->t('Session data removed - all users logged out.'));
    return TRUE;
  }

}
