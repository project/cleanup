<?php

namespace Drupal\cleanup\Plugin\CleanupTask;

use Drupal\cleanup\CleanupTaskInterface;
use Drupal\cleanup\ConfigurableCleanupTaskBase;

/**
 * Truncates the logs stored by the database logging module.
 *
 * @CleanupTask(
 *   id = "database_logs",
 *   label = @Translation("Purge database logs"),
 *   description = @Translation("Truncates the logs stored by the database
 *   logging module."),
 * )
 */
class DatabaseLogs extends ConfigurableCleanupTaskBase implements CleanupTaskInterface {

  /**
   * {@inheritdoc}
   */
  public function runCleanup() {
    $logger = $this->container->get('messenger');
    $database = $this->container->get('database');

    if (!$database->schema()->tableExists('watchdog')) {
      $logger->addMessage($this->t('No database log table found - nothing to be cleaned.'));
      return TRUE;
    }

    $database->truncate('watchdog')->execute();
    $logger->addMessage($this->t('Database logs truncated.'));
    return TRUE;
  }

}
