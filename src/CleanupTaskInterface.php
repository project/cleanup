<?php

namespace Drupal\cleanup;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines the interface for cleanup tasks.
 *
 * @see \Drupal\cleanup\Annotation\CleanupTask
 * @see \Drupal\cleanup\CleanupTaskBase
 * @see \Drupal\cleanup\ConfigurableCleanupTaskInterface
 * @see \Drupal\cleanup\ConfigurableCleanupTaskBase
 * @see \Drupal\cleanup\CleanupTaskManager
 * @see plugin_api
 */
interface CleanupTaskInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface, ConfigurablePluginInterface {

  /**
   * Runs a cleanup task.
   *
   * @return bool
   *   TRUE on success. FALSE if unable to perform the task.
   */
  public function runCleanup();

  /**
   * Returns a render array summarizing the configuration of the task.
   *
   * @return array
   *   A render array.
   */
  public function getSummary();

  /**
   * Returns the task label.
   *
   * @return string
   *   The cleanup task label.
   */
  public function label();

  /**
   * Returns the unique ID representing the cleanup task.
   *
   * @return string
   *   The cleanup task ID.
   */
  public function getUuid();

  /**
   * Returns the weight of the cleanup task.
   *
   * @return int|string
   *   Either the integer weight of the cleanup task, or an empty string.
   */
  public function getWeight();

  /**
   * Sets the weight for this cleanup task.
   *
   * @param int $weight
   *   The weight for this cleanup task.
   *
   * @return $this
   */
  public function setWeight($weight);

}
