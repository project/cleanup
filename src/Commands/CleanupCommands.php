<?php

namespace Drupal\cleanup\Commands;

use Drupal\Core\Entity\EntityTypeManager;
use Drush\Commands\DrushCommands;

/**
 * Cleanup command for Drush 9+.
 *
 * @package Drupal\cleanup\Commands
 */
class CleanupCommands extends DrushCommands {

  /**
   * Entity Type manager.
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * ViewsBulkOperationsCommands object constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user object.
   * @param \Drupal\views_bulk_operations\Service\ViewsbulkOperationsViewDataInterface $viewData
   *   VBO View data service.
   * @param \Drupal\views_bulk_operations\Service\ViewsBulkOperationsActionManager $actionManager
   *   VBO Action manager service.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Run a cleanup from the command line.
   *
   * @param string $configuration_name
   *   The name of the configuration to be run.
   * @param array $options
   *   Options supplied to the command.
   *
   * @command cleanup:clean
   *
   * @aliases clean,cleanup
   * @usage drush clean myconfig
   *   Run the 'myconfig' cleanup.
   */
  public function cleanup($configuration_name = '', array $options = ['cleanup' => NULL]) {

    $storage = $this->entityTypeManager
      ->getStorage('cleanup');

    if (empty($configuration_name)) {
      $cleanups = $storage->loadMultiple();
      if (empty($cleanups)) {
        $this->logger->notice(dt('No cleanup task lists have been created yet. Visit /admin/config/cleanup to make one.'));
        return;
      }

      $count = count($cleanups);
      $conjunction = '';
      $index = 1;
      $list = '';
      foreach ($cleanups as $name => $instance) {
        $list .= $conjunction . $name;
        $index++;
        $conjunction = ($index == $count) ? ' and ' : ', ';
      }
      $this->logger->notice(dt('The following cleanup task lists are available: %list.', ['%list' => $list]));
      return;
    }

    $task_list = $storage->load($configuration_name);

    if (empty($task_list)) {
      $this->logger->notice(dt("The '%name' task list doesn't exist.", ['%name' => $configuration_name]));
      return;
    }

    $task_list->runCleanup();
    $this->logger->notice(dt("Cleanup %name completed.", ['%name' => $configuration_name]));
  }

}
