<?php

namespace Drupal\cleanup;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of cleanup task list entities.
 *
 * @see \Drupal\cleanup\Entity\Cleanup
 */
class CleanupListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Cleanup name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {

    $operations = parent::getDefaultOperations($entity);

    if ($entity->getTasks()->count()) {
      $run = [
        'title' => $this->t('Run'),
        'weight' => 200,
        'url' => $entity->toUrl('run'),
      ];

      $operations += [
        'run' => $run,
      ];
    }

    if (isset($operations['edit'])) {
      $operations['edit']['url'] = $entity->toUrl('edit-form');
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('There are currently no task lists. <a href=":url">Add a new one</a>.', [
      ':url' => Url::fromRoute('cleanup.tasklist_add')->toString(),
    ]);
    return $build;
  }

}
