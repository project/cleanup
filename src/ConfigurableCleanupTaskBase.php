<?php

namespace Drupal\cleanup;

use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for configurable cleanup tasks.
 *
 * @see \Drupal\cleanup\Annotation\CleanupTask
 * @see \Drupal\cleanup\ConfigurableCleanupTaskInterface
 * @see \Drupal\cleanup\CleanupTaskInterface
 * @see \Drupal\cleanup\CleanupTaskBase
 * @see \Drupal\cleanup\CleanupTaskManager
 * @see plugin_api
 */
abstract class ConfigurableCleanupTaskBase extends CleanupTaskBase implements ConfigurableCleanupTaskInterface {

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function hasConfigurationForm() {
    $formState = new FormState();
    $form = $this->buildConfigurationForm([], $formState);
    return (!empty($form));
  }

}
