<?php

namespace Drupal\cleanup;

use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the interface for configurable cleanup tasks.
 *
 * @see \Drupal\cleanup\Annotation\CleanupTask
 * @see \Drupal\cleanup\ConfigurableCleanupTaskBase
 * @see \Drupal\cleanup\CleanupTaskInterface
 * @see \Drupal\cleanup\CleanupTaskBase
 * @see \Drupal\cleanup\CleanupTaskManager
 * @see plugin_api
 */
interface ConfigurableCleanupTaskInterface extends CleanupTaskInterface, PluginFormInterface {
}
