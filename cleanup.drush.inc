<?php

use Drush\Drush;

/**
 * @file
 * Define drush command for cleaning up a database.
 */

/**
 * Implements hook_drush_command().
 */
function cleanup_drush_command() {
  $items = [];
  $items['cleanup'] = [
    'description' => 'Run a cleanup',
    'drupal dependencies' => ['cleanup'],
    'aliases' => ['clean', 'cleanup'],
    'arguments' => ['configuration' => 'The machine name of the configuration to use'],
  ];
  return $items;
}

/**
 * Call back function cleanup_run()
 *
 * The call back function name in the  following format
 *   drush_{module_name}_{item_id_for_command}()
 */
function cleanup_cleanup($configuration) {
  Drush::logger()->error('Cleanup ran!');
}
